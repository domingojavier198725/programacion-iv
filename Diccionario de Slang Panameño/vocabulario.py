""" Elaborar una aplicación de línea de comandos en Python que sirva cuyo propósito
 sea mantener un diccionario de palabras del slang panameño (xopa, mopri, otras).
 Las palabras y su significado deben ser almacenadas dentro de una base de datos SQLite.
 Las opciones dentro del programa deben incluir como mínimo: a) Agregar nueva palabra,
 c) Editar palabra existente, d) Eliminar palabra existente, e) Ver listado de palabras,
 f) Buscar significado de palabra, g) Salir"""

import sys
import sqlite3

def slang_panameño():
    palabra = input("Digite la palabra del slang panameño: ")
    definicion = input("Digite la definicion de la palabra: ")
    con = sqlite3.connect('slang.db')
    cursor = con.cursor()
    cursor.execute("insert into diccionario (palabra,definicion) values ('"+palabra+"','"+definicion+"')")
    con.commit()
    con.close()
    opciones()

def editar_palabra():
    diccionario = []
    con = sqlite3.connect('slang.db')
    cursor = con.cursor()
    cursor.execute("select * from diccionario")
    print("\t Palabras agregadas: ")
    print("\t codigo \t palabra  \t definicion")
    for diccionari in cursor:
        diccionario.append(diccionari)
        dic = '\t ' + str(diccionari[0]) + '\t ' + diccionari[1] + ' \t ' + diccionari[2]
        print(dic)
    codigo = input("Ingrese el codigo de la palabra a editar: ")
    for diccionari in diccionario:
        print(diccionari[0])
        if int(diccionari[0]) == int(codigo):
            palabra= diccionari[1]
            definicion = diccionari[2]
            break
    palabra = input("Escriba la nueva palabra "+palabra+": ")
    definicion = input("Escriba la nueva definicion "+definicion+":")
    sql = "update diccionario set palabra='"+palabra+"',definicion='"+definicion+"' where codigo= " + codigo
    cursor.execute(sql)
    con.commit()
    con.close()
    print("Palabra editada :) ")
    input()
    opciones()

def eliminar_palabra():
    con = sqlite3.connect('slang.db')
    cursor = con.cursor()
    cursor.execute("select * from diccionario")
    print("\t Palabras agregadas: ")
    print("\t codigo \t palabra  \t definicion")
    for diccionari in cursor:
        dic = '\t '+str(diccionari[0])+'\t '+diccionari[1]+' \t '+diccionari[2]
        print(dic)
    codigo = input("Ingrese el codigo de la palabra a borrar: ")
    sql = "delete from diccionario where codigo= "+codigo
    cursor.execute(sql)
    con.commit()
    con.close()
    print("Palabra borrada")
    input()
    opciones()

def ver_palabras():
    con = sqlite3.connect('slang.db')
    cursor = con.cursor()
    cursor.execute("select * from diccionario")
    print("\t Palabras agregadas: ")
    print("\t palabra ")
    print("___________________________________")
    for diccionari in cursor:
        dic = '\t '+diccionari[1]
        print(dic)
    con.close()
    input()
    opciones()

def buscar_significado():
    con = sqlite3.connect('slang.db')
    cursor = con.cursor()
    cursor.execute("select * from diccionario")
    print("\t Palabras agregadas con su respectivo significado: ")
    print("\t palabra \t definicion")
    print("___________________________________")
    for diccionari in cursor:
        dic = '\t ' + diccionari[1] + ' \t ' + diccionari[2]
        print(dic)
    con.close()
    input()
    opciones()

def opciones():
    print("1- Agregar nueva palabra")
    print("2- Editar palabra existente")
    print("3- Eliminar palabra existente")
    print("4- Ver listado de palabras")
    print("5- Buscar significado de palabras")
    print("6- Salir")
    opcion=input("Seleccione una opcion:")
    if opcion=='1':
        slang_panameño()
    elif opcion == '2':
        editar_palabra()
    elif opcion=='3':
        eliminar_palabra()
    elif opcion=='4':
        ver_palabras()
    elif opcion=='5':
        buscar_significado()
    elif opcion=='6':
         print("Regrese pronto:")
         input()
         sys.exit()
    else:
        input("Seleccione una opcion valida !!!")
        opciones()

opciones()




